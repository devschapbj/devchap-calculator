package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.example.calculator.databinding.ActivityMainBinding
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)

        /*switch = findViewById(R.id.switchTheme)
        switch.setOnCheckedChangeListener{btn, isChecked ->
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }*/

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.switchTheme.setOnCheckedChangeListener { btn, isChecked ->
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

        binding.tvOne.setOnClickListener { appendOnExpresstion("1", true, "operating") }
        binding.tvTwo.setOnClickListener { appendOnExpresstion("2", true, "operating") }
        binding.tvThree.setOnClickListener { appendOnExpresstion("3", true, "operating") }
        binding.tvFour.setOnClickListener { appendOnExpresstion("4", true, "operating") }
        binding.tvFive.setOnClickListener { appendOnExpresstion("5", true, "operating") }
        binding.tvSix.setOnClickListener { appendOnExpresstion("6", true, "operating") }
        binding.tvSeven.setOnClickListener { appendOnExpresstion("7", true, "operating") }
        binding.tvHeight.setOnClickListener { appendOnExpresstion("8", true, "operating") }
        binding.tvNine.setOnClickListener { appendOnExpresstion("9", true, "operating") }
        binding.tvZero.setOnClickListener { appendOnExpresstion("0", true, "operating") }
        binding.tvDot.setOnClickListener { appendOnExpresstion(".", true, "operating") }

        binding.tvDivide.setOnClickListener { appendOnExpresstion("/", true, "operator") }
        binding.tvMultiplication.setOnClickListener { appendOnExpresstion("*", true, "operator") }
        binding.tvOpeNegative.setOnClickListener { appendOnExpresstion("-", true, "operator") }
        binding.tvOpePlus.setOnClickListener { appendOnExpresstion("+", true, "operator") }

        binding.tvNegative.setOnClickListener { appendOnExpresstion("-", true, "moreOrLess") }

        binding.tvClear.setOnClickListener {
            binding.tvOperation.text = ""
            binding.tvResult.text = ""
        }


        binding.tvEqual.setOnClickListener {
            calculate()
        }

        binding.tvPercent.setOnClickListener {
            calculate()
            var percent: Float = binding.tvResult.text.toString().toFloat() / 100
            binding.tvResult.text = percent.toString()
            binding.tvOperation.text = ""
        }

    }

    private fun calculate () {
        try {
            val expression = ExpressionBuilder(binding.tvOperation.text.toString()).build()
            val result = expression.evaluate()
            val longResult = result.toLong()

            if (result == longResult.toDouble()) {
                binding.tvResult.text = longResult.toString()
            } else {
                binding.tvResult.text = result.toString()
            }
        } catch (e: Exception) {
            Log.e("Exception"," message : " + e.message )
        }
    }

    private fun appendOnExpresstion (string: String, canClear: Boolean, type: String) {
        var unauthorizeOperator: Array<String> = arrayOf("/", "*", "+", "-")
        if (binding.tvResult.text.isNotEmpty()) {
            binding.tvOperation.text = ""
        }

        if (canClear) {
            binding.tvResult.text = ""

            var lastChar: String = ""
            if (binding.tvOperation.text.isNotEmpty()) {
                lastChar = binding.tvOperation.text.last().toString()
            }

            when (type) {
                "operator" -> {
                    if (binding.tvOperation.text.isEmpty()) {
                        return
                    }

                    if (unauthorizeOperator.contains(lastChar)) {
                        binding.tvOperation.text = binding.tvOperation.text.substring(0, binding.tvOperation.text.length - 1)
                        binding.tvOperation.append(string)
                    } else {
                        binding.tvOperation.append(string)
                    }

                }
                "operating" -> {
                    binding.tvOperation.append(string)
                }
                "moreOrLess" -> {
                    if (binding.tvOperation.text.isNotEmpty() && binding.tvOperation.text.first().toString() == "-") {

                        var getOperationVal = binding.tvOperation.text.toString().drop(1)
                        binding.tvOperation.text = getOperationVal
                    } else {
                        var str = StringBuilder(binding.tvOperation.text)
                        str.insert(0, string)
                        binding.tvOperation.text = ""
                        binding.tvOperation.text = str.toString()
                    }
                }
            }

        } else {
            binding.tvOperation.append(binding.tvResult.text)
            binding.tvOperation.append(string)
            binding.tvResult.text = ""
        }
    }
}